import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DesktopLayoutComponent } from './components/layouts/desktop-layout/desktop-layout.component';
import { LargeDeviceLayoutComponent } from './components/layouts/large-device-layout/large-device-layout.component';
import { MediumDeviceLayoutComponent } from './components/layouts/medium-device-layout/medium-device-layout.component';
import { SmallDeviceLayoutComponent } from './components/layouts/small-device-layout/small-device-layout.component';
import { ExtraSmallDeviceLayoutComponent } from './components/layouts/extra-small-device-layout/extra-small-device-layout.component';
import { ResponsiveLayoutComponent } from './components/layouts/responsive-layout/responsive-layout.component';

const ROUTES: Routes = [
  { path: '', redirectTo: 'desktop-layout', pathMatch: 'full' },
  { path: 'desktop-layout', component: DesktopLayoutComponent },
  { path: 'large-device-layout', component: LargeDeviceLayoutComponent },
  { path: 'medium-device-layout', component: MediumDeviceLayoutComponent },
  { path: 'small-device-layout', component: SmallDeviceLayoutComponent },
  { path: 'extra-small-device-layout', component: ExtraSmallDeviceLayoutComponent },
  { path: 'responsive-layout', component: ResponsiveLayoutComponent },
  { path: '**', redirectTo: 'desktop-layout' }
]

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, { useHash: true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
