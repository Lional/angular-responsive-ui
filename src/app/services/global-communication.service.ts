import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, ReplaySubject, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

class EventData {
  id: string;
  data: any;
  constructor(id: string, data?: any) {
    this.id = id;
    this.data = data || null;
  }
}

@Injectable({
  providedIn: 'root'
})

export class GlobalCommunicationService {

  constructor() { }
  private dataSourceS = new Subject<any>();
  sendDataS(data: any) {
    this.dataSourceS.next(data);
  }
  receiveDataS() {
    return this.dataSourceS.asObservable();
  }


  private dataSourceBS = new BehaviorSubject<any>(null);
  sendDataBS(data: any) {
    this.dataSourceBS.next(data);
  }
  receiveDataBS() {
    return this.dataSourceBS.asObservable();
  }

  private dataSourceRS = new ReplaySubject<any>(3);
  sendDataRS(data: any) {
    this.dataSourceRS.next(data);
  }
  receiveDataRS() {
    return this.dataSourceRS.asObservable();
  }

  /**
   * very effective communication way using emit and on
   */
  private subject$ = new Subject();
  emit(id: string, data?: any) {
    let event: any = new EventData(id, data);
    this.subject$.next(event);
  }
  on(eventName: string, action: any): Subscription {
    return this.subject$.pipe(
      filter((event: any) => event.id === eventName),
      map((event: any) => event['data'])
    ).subscribe(action);
  }
  // very effective communication way using emit and on ends here
}



