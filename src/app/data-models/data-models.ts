export interface ButtonItem {
  label: string;
  screen: string;
  active?: boolean;
  routePath?: string
}
