import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ButtonItem } from 'src/app/data-models/data-models';
import { GlobalCommunicationService } from 'src/app/services/global-communication.service';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit {
  buttons: ButtonItem[] = [
    {
      label: 'Desktop',
      screen: 'desktop',
      active: false,
      routePath: 'desktop-layout'
    },
    {
      label: 'Large Device',
      screen: 'lg',
      active: false,
      routePath: 'large-device-layout'
    },
    {
      label: 'Medium Device',
      screen: 'md',
      active: false,
      routePath: 'medium-device-layout'
    },
    {
      label: 'Small Device',
      screen: 'sm',
      active: false,
      routePath: 'small-device-layout'
    },
    {
      label: 'Extra Small Device',
      screen: 'xs',
      active: false,
      routePath: 'extra-small-device-layout'
    },
    {
      label: 'Responsive',
      screen: 'responsive',
      active: false,
      routePath: 'responsive-layout'
    }
  ]

  showInfo: boolean = false;

  constructor(
    private globalCommunicationService: GlobalCommunicationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        for (let i = 0; i < this.buttons.length; i++) {
          if (val.urlAfterRedirects.indexOf(this.buttons[i].routePath || '') !== -1) {
            this.globalCommunicationService.emit('top-panel', this.buttons[i].label);
            if (this.buttons[i].screen === 'responsive') {
              this.showInfo = true;
            }
            break;
          }
        }
      }
    });
  }

  switchScreen(button: ButtonItem) {
    // sending the label as title to top-panel
    this.globalCommunicationService.emit('top-panel', button.label);
    this.showInfo = false;
    if (button.screen === 'responsive') {
      this.showInfo = true;
    }
  }

}
