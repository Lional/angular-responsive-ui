import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { GlobalCommunicationService } from 'src/app/services/global-communication.service';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.scss']
})
export class TopPanelComponent implements OnInit {
  title: string = 'DESKTOP';

  subscription: Subscription[] = [];

  constructor(private globalCommunicationService: GlobalCommunicationService) { }

  ngOnInit(): void {
    // push subscription to array
    this.subscription.push(this.globalCommunicationService.on('top-panel', (title: string) => {
      this.title = title;
    }));
  }

  ngOnDestroy() {
    // loop all subscription to unsubscribe when component destroy
    this.subscription.forEach(subscribe => {
      subscribe.unsubscribe();
    });
  }

}
