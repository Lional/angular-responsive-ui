import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-large-device-layout',
  templateUrl: './large-device-layout.component.html',
  styleUrls: ['./large-device-layout.component.scss']
})
export class LargeDeviceLayoutComponent implements OnInit {
  /**
   * @property
   * @description it represents the count of tiles in array format
   */
  tiles: any = new Array(10);

  constructor() { }

  ngOnInit(): void {
  }

}
