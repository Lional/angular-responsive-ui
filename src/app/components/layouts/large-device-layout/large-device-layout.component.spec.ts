import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LargeDeviceLayoutComponent } from './large-device-layout.component';

describe('LargeDeviceLayoutComponent', () => {
  let component: LargeDeviceLayoutComponent;
  let fixture: ComponentFixture<LargeDeviceLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LargeDeviceLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LargeDeviceLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
