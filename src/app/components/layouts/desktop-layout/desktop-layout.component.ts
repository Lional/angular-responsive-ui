import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-desktop-layout',
  templateUrl: './desktop-layout.component.html',
  styleUrls: ['./desktop-layout.component.scss']
})
export class DesktopLayoutComponent implements OnInit {
  /**
   * @property
   * @description it represents the count of tiles in array format
   */
  tiles: any = new Array(10);

  constructor() { }

  ngOnInit(): void {
  }

}
