import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsiveLayoutComponent } from './responsive-layout.component';

describe('ResponsiveLayoutComponent', () => {
  let component: ResponsiveLayoutComponent;
  let fixture: ComponentFixture<ResponsiveLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResponsiveLayoutComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsiveLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('set true to desktop when window size greater than 1400px', () => {
    component.setScreen(1401);
    let desktop = component.desktop
    expect(desktop).toBe(true);
  });

  it('set true to fourCol when window size lesser than 1400px and greater than 1250px', () => {
    component.setScreen(1300);
    let fourCol = component.fourCol
    expect(fourCol).toBe(true);
  });

  it('set true to threeCol when window size lesser than 1250px and greater than 1000px', () => {
    component.setScreen(1001);
    let threeCol = component.threeCol
    expect(threeCol).toBe(true);
  });

  it('set true to twoCol when window size lesser than 1000px and greater than 790px', () => {
    component.setScreen(800);
    let twoCol = component.twoCol
    expect(twoCol).toBe(true);
  });

  it('set true to oneCol when window size lesser than 790px', () => {
    component.setScreen(600);
    let oneCol = component.oneCol
    expect(oneCol).toBe(true);
  });

  it('set all layout value false', () => {
    component.setAllFalse();
    let oneCol = component.oneCol;
    let twoCall = component.twoCol;
    let threeCol = component.threeCol;
    let fourCol = component.fourCol;
    let desktop = component.desktop;
    let allFalse = !oneCol && !twoCall && !threeCol && !fourCol && !desktop
    expect(allFalse).toBe(true);
  });
});
