import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-responsive-layout',
  templateUrl: './responsive-layout.component.html',
  styleUrls: ['./responsive-layout.component.scss']
})
export class ResponsiveLayoutComponent implements OnInit {
  /**
   * @property
   * @type {Array}
   * @description it represents the count of tiles in array format
   */
  tiles: Array<null> = new Array(10);

  /**
   * @property
   * @type {number}
   * @description to store the viewport width
   */
  windowWidth: number = 0;

  /**
   * @property
   * @type {boolean}
   * @description to check is the viewport set to desktop width
   */
  desktop: boolean = false;

  /**
   * @property
   * @type {boolean}
   * @description to check is the viewport set to four column layout width
   */
  fourCol: boolean = false;

  /**
   * @property
   * @type {boolean}
   * @description to check is the viewport set to three column layout width
   */
  threeCol: boolean = false;

  /**
   * @property
   * @type {boolean}
   * @description to check is the viewport set to two column layout width
   */
  twoCol: boolean = false;

  /**
   * @property
   * @type {boolean}
   * @description to check is the viewport set to one column layout width
   */
  oneCol: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.windowWidth = window.innerWidth;
    this.setScreen(this.windowWidth);
  }
  /**
   * @method windowResize
   * @description this function represents to identify the window or viewport width
   * @param $event
   * @returns {number} windowWidth
   */
  windowResize($event: any) {
    this.windowWidth = $event.target.innerWidth;
    this.setScreen(this.windowWidth);
  }

  /**
   * @method setScreen
   * @description this function represents to change the required boolean value to identify the screen width
   * @param {number} windowWidth
   */
  setScreen(windowWidth: number) {
    this.setAllFalse();
    if (windowWidth > 1400) {
      this.desktop = true;
    } else if (windowWidth <= 1400 && windowWidth > 1250) {
      this.fourCol = true;
    } else if (windowWidth <= 1250 && windowWidth > 1000) {
      this.threeCol = true;
    } else if (windowWidth <= 1000 && windowWidth > 790) {
      this.twoCol = true;
    } else if (windowWidth <= 790) {
      this.oneCol = true;
    }
  }

  /**
   * @method setAllFalse
   * @description this function change all column layout boolean variable to false
   */
  setAllFalse() {
    this.desktop = false;
    this.fourCol = false;
    this.threeCol = false;
    this.twoCol = false;
    this.oneCol = false;
  }

}
