import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medium-device-layout',
  templateUrl: './medium-device-layout.component.html',
  styleUrls: ['./medium-device-layout.component.scss']
})
export class MediumDeviceLayoutComponent implements OnInit {
  /**
   * @property
   * @description it represents the count of tiles in array format
   */
  tiles: any = new Array(10);

  constructor() { }

  ngOnInit(): void {
  }

}
