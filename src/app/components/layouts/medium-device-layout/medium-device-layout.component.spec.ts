import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediumDeviceLayoutComponent } from './medium-device-layout.component';

describe('MediumDeviceLayoutComponent', () => {
  let component: MediumDeviceLayoutComponent;
  let fixture: ComponentFixture<MediumDeviceLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediumDeviceLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediumDeviceLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
