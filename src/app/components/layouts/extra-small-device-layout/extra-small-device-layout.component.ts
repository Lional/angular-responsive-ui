import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-extra-small-device-layout',
  templateUrl: './extra-small-device-layout.component.html',
  styleUrls: ['./extra-small-device-layout.component.scss']
})
export class ExtraSmallDeviceLayoutComponent implements OnInit {
  /**
   * @property
   * @description it represents the count of tiles in array format
   */
  tiles: any = new Array(10);

  constructor() { }

  ngOnInit(): void {
  }

}
