import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraSmallDeviceLayoutComponent } from './extra-small-device-layout.component';

describe('ExtraSmallDeviceLayoutComponent', () => {
  let component: ExtraSmallDeviceLayoutComponent;
  let fixture: ComponentFixture<ExtraSmallDeviceLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtraSmallDeviceLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraSmallDeviceLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
