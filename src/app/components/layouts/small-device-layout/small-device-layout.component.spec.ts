import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallDeviceLayoutComponent } from './small-device-layout.component';

describe('SmallDeviceLayoutComponent', () => {
  let component: SmallDeviceLayoutComponent;
  let fixture: ComponentFixture<SmallDeviceLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmallDeviceLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallDeviceLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
