import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-small-device-layout',
  templateUrl: './small-device-layout.component.html',
  styleUrls: ['./small-device-layout.component.scss']
})
export class SmallDeviceLayoutComponent implements OnInit {
  /**
   * @property
   * @description it represents the count of tiles in array format
   */
  tiles: any = new Array(10);

  constructor() { }

  ngOnInit(): void {
  }

}
