import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TopPanelComponent } from './components/top-panel/top-panel.component';
import { LeftPanelComponent } from './components/left-panel/left-panel.component';
import { MainPanelComponent } from './components/main-panel/main-panel.component';
import { RouterPanelComponent } from './components/router-panel/router-panel.component';
import { DesktopLayoutComponent } from './components/layouts/desktop-layout/desktop-layout.component';
import { TileComponent } from './components/tile/tile.component';
import { LargeDeviceLayoutComponent } from './components/layouts/large-device-layout/large-device-layout.component';
import { AppRoutingModule } from './app.routing';
import { MediumDeviceLayoutComponent } from './components/layouts/medium-device-layout/medium-device-layout.component';
import { SmallDeviceLayoutComponent } from './components/layouts/small-device-layout/small-device-layout.component';
import { ExtraSmallDeviceLayoutComponent } from './components/layouts/extra-small-device-layout/extra-small-device-layout.component';
import { ResponsiveLayoutComponent } from './components/layouts/responsive-layout/responsive-layout.component';

import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    TopPanelComponent,
    LeftPanelComponent,
    MainPanelComponent,
    RouterPanelComponent,
    DesktopLayoutComponent,
    TileComponent,
    LargeDeviceLayoutComponent,
    MediumDeviceLayoutComponent,
    SmallDeviceLayoutComponent,
    ExtraSmallDeviceLayoutComponent,
    ResponsiveLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
