# AngularResponsiveUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

# README #

This Project created to demonstrate a same design of layout to different viewport sizes.

### What is this repository for? ###

* Design Test
* version 1.0.0

### How do I get set up? ###

* clone this repo to local machine
* run => "npm i" in command window by navigate to project directory
* note: you need node and angular cli to run this project
* angular => https://angular.io/cli 
* node: https://nodejs.org/en/

### Contribution guidelines ###

* To run unit testing please run => "ng test"

### Who do I talk to? ###

* For any queries please reach to jpleoleo@gmail.com
